<?php
include_once($_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'AtomicProject_Mazharul_107369_B11' . DIRECTORY_SEPARATOR . 'views' . DIRECTORY_SEPARATOR . 'startup.php');

use \App\Bitm\SEIP107369\Education\EducationLevel;
use \App\Bitm\SEIP107369\Utility\Utility;

$new_education = new EducationLevel();
$education = $new_education->show($_GET['id']);
?>


<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Education Level</title>

        <!-- Bootstrap -->
        <link href="./../../../asset/css/bootstrap.min.css" rel="stylesheet">
        <link href="./../../../asset/css/custom.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <!--font-->
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
    </head>
    <body class="bg-primary">
        <section>
            <div class="container">
                <div class="row">
                    <h3 class="text-center">Update Education Level</h3>
                    <hr>
                    <form class="form-inline text-center" action="update.php" method="post">
                        <div class="form-group">
                            <label for="exampleInputName2">Chose Education Level: </label>
                            <input type="hidden" class="form-control"  name="id" id="exampleInputName2" value="<?php echo $education['id']; ?>" >
                            <br>
                            <input type="text" class="form-control"  name="title" id="exampleInputName2" value="<?php echo $education['education_level']; ?>" >
                            <label class="radio-inline">
                                <input type="radio" name="title" id="inlineRadio1" value="M.Sc"> M.Sc
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="title" id="inlineRadio2" value="B.Sc"> B.Sc
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="title" id="inlineRadio3" value="HSC"> HSC
                            </label>


                            <button type="submit" class="btn btn-warning btn-md">Update</button>
                    </form>
                </div>
            </div>
        </section>

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="./../../../asset/js/bootstrap.min.js"></script>

    </body>
</html>