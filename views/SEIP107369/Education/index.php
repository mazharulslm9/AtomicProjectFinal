<?php
include_once($_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'AtomicProject_Mazharul_107369_B11' . DIRECTORY_SEPARATOR . 'views' . DIRECTORY_SEPARATOR . 'startup.php');

use App\Bitm\SEIP107369\Education\EducationLevel;
use App\Bitm\SEIP107369\Utility\Utility;

$new_education = new EducationLevel();
$education = $new_education->index();
?>



<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Education Level</title>

        <!-- Bootstrap -->
        <link href="./../../../asset/css/bootstrap.min.css" rel="stylesheet">
        <link href="./../../../asset/css/custom.css" rel="stylesheet">


        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <!--font-->
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
    </head>
    <body class="bg-primary">
        <section>
            <div class="container">
                <h3 class="text-center">Education Level List</h3>
                <hr>
                <div class="row">
                    <div class="col-md-3">
                        <button  class="btn btn-info btn-md"> <a href="create.php">Add Education Level</a></button>
                    </div><!-- /.col-lg-6 -->
                    <div class="col-md-3">
                        <select style="width: 50%;color: blue">
                            <option>10</option>
                            <option>20</option>
                            <option>30</option>
                            <option>40</option>
                            <option>50</option>
                        </select>
                    </div>
                    <div class="col-md-3">
                        <button  class="btn btn-danger btn-md">Download as PDF/XL</button>

                    </div>
                    <div class="col-md-3">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Search for...">
                            <span class="input-group-btn">
                                <button class="btn btn-default" type="button">Go!</button>
                            </span>
                        </div><!-- /input-group -->
                    </div><!-- /.col-lg-6 -->
                </div><!-- /.row -->

                <hr>
                <div style="width: 50%;margin: 0 auto;margin: 10px;" class="row alert alert-success" id="message" >
                    <?php echo Utility::message(); ?>


                </div>

                <table class="table table-bordered text-center">
                    <thead class="text-center">


                        <tr >
                            <th class="text-center">SL NO</th>
                            <th class="text-center">Education Level &dArr;</th>
                            <th class="text-center" colspan="5">Action</th>

                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $slno = 1;
                        foreach ($education as $edu) {
                            ?>
                            <tr>
                                <td><?php echo $slno; ?></td>
    <!--                                <td><?php echo $edu->id; ?></td>-->
                                <td style="text-transform: uppercase"><a href="show.php?id=<?php echo $edu->id; ?>"><?php echo $edu->education_level; ?></a></td>
                                <td><button class="btn btn-success btn-md"><a href="show.php?id=<?php echo $edu->id; ?>">View</a></button></td>
                                <td><button class="btn btn-warning btn-md"><a href="edit.php?id=<?php echo $edu->id; ?>">Edit</a></button></td>



                        <form action="delete.php" method="post">
                            <input type="hidden" name="id" value="<?php echo $edu->id; ?>"/>
                            <td><button type="submit" class="btn btn-danger btn-md delete">Delete</button></td>
                        </form>
                        <td><button class="btn btn-danger btn-md"><a href="#">Trash/Recover </a></button></td>
                        <td><button class="btn btn-danger btn-md"><a href="#"> Email to Friend </a></button></td>


                        </tr>
                        <?php
                        $slno++;
                    }
                    ?>
                    </tbody>

                </table>
            </div>
            <div class="row text-center"> 
                <button class="btn btn-danger btn-md"><a href="./../../../index.php"> Back To Project List </a></button>
                <nav>
                    <ul class="pagination">
                        <li>
                            <a href="#" aria-label="Previous">
                                <span aria-hidden="true">&laquo;</span>
                            </a>
                        </li>
                        <li><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li><a href="#">5</a></li>
                        <li>
                            <a href="#" aria-label="Next">
                                <span aria-hidden="true">&raquo;</span>
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>

        </section>

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="./../../../asset/js/bootstrap.min.js"></script>
        <script>
            $('.delete').bind('click', function (e) {

                var deleteItem = confirm("Are You Sure You Want to Delete?");
                if (!deleteItem) {

                    //return false;
                    e.preventDefault();
                }


            });
            $('#message').fadeOut(6000);

        </script>

    </body>
</html>
