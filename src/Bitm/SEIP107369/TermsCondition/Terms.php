<?php

namespace App\Bitm\SEIP107369\TermsCondition;

use App\Bitm\SEIP107369\Utility\Utility;

class Terms {

    public $id = " ";
    public $title = " ";
    public $created = " ";
    public $modified = " ";
    public $created_by = " ";
    public $modified_by = " ";
    public $deleted_at = " ";

    public function __construct($data = false) {
        if (is_array($data) && array_key_exists('id', $data) && !empty($data['id'])) {
            $this->id = $data['id'];
        }
        $this->title = $data['title'];
    }

    public function show($id = false) {
        $conn = mysql_connect("localhost", "root", "") or die("Cannot connect database.");
        $lnk = mysql_select_db("new") or die("Cannot select database.");

        $query = "SELECT * FROM `terms_tbl` WHERE id=" . $id;
        $result = mysql_query($query);
        $row = mysql_fetch_assoc($result);
        return $row;
    }

    public function index() {
        $term = array();

        $conn = mysql_connect("localhost", "root", "") or die("Cannot connect database.");
        $lnk = mysql_select_db("new") or die("Cannot select database.");

        $query = "SELECT * FROM `terms_tbl`";
        $result = mysql_query($query);

        while ($row = mysql_fetch_object($result)) {
            $term[] = $row;
        }
        return $term;
    }

    public function create() {
        echo 'I am create form';
    }

    public function store() {
        $conn = mysql_connect("localhost", "root", "") or die("Cannot connect database.");
        $lnk = mysql_select_db("new") or die("Cannot select database.");

        $query = "INSERT INTO `new`.`terms_tbl` (`terms`) VALUES ('" . $this->title . "');";

        $result = mysql_query($query);


        if ($result) {
            Utility::message("<strong>$this->title</strong>  is added successfully.");
        } else {
            Utility::message("There is an error while saving data. Please try again later.");
        }

        Utility::redirect('index.php');
    }

    public function edit() {
        echo 'I am editing form';
    }

    public function update() {
        $conn = mysql_connect("localhost", "root", "") or die("Cannot connect database.");
        $lnk = mysql_select_db("new") or die("Cannot select database.");

        $query = "UPDATE `new`.`terms_tbl` SET `terms` = '" . $this->title . "' WHERE `terms_tbl`.`id` = " . $this->id;
        $result = mysql_query($query);

        if ($result) {
            Utility::message("Term is edited successfully.");
        } else {
            Utility::message("There is an error while saving data. Please try again later.");
        }

        Utility::redirect('index.php');
    }

    public function delete($id=NULL) {
        if (is_null($id)) {
            Utility::message("No Id Found!!");
            return Utility::redirect("index.php");
        }

        $conn = mysql_connect("localhost", "root", "") or die("Cannot connect database.");
        $lnk = mysql_select_db("new") or die("Cannot select database.");
        $query = "DELETE FROM `new`.`terms_tbl` WHERE `terms_tbl`.`id` = " . $id;
        $result = mysql_query($query);


        if ($result) {
            Utility::message("Term deleted successfully.");
        } else {
            Utility::message("There is an error while deleting title. Please try again later.");
        }

        Utility::redirect('index.php');
    }

}
