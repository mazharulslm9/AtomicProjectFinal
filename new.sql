-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 06, 2016 at 07:15 PM
-- Server version: 5.6.20
-- PHP Version: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `new`
--

-- --------------------------------------------------------

--
-- Table structure for table `birthday_tbl`
--

CREATE TABLE IF NOT EXISTS `birthday_tbl` (
`id` int(11) NOT NULL,
  `birthdate` date NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `birthday_tbl`
--

INSERT INTO `birthday_tbl` (`id`, `birthdate`) VALUES
(2, '2010-02-03'),
(3, '2015-12-29'),
(4, '2015-12-11'),
(5, '1991-02-02'),
(6, '2011-02-17'),
(7, '2016-01-02');

-- --------------------------------------------------------

--
-- Table structure for table `city_tbl`
--

CREATE TABLE IF NOT EXISTS `city_tbl` (
`id` int(11) NOT NULL,
  `city_name` varchar(255) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `city_tbl`
--

INSERT INTO `city_tbl` (`id`, `city_name`) VALUES
(7, 'Dhaka');

-- --------------------------------------------------------

--
-- Table structure for table `education_tbl`
--

CREATE TABLE IF NOT EXISTS `education_tbl` (
`id` int(11) NOT NULL,
  `education_level` varchar(255) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `education_tbl`
--

INSERT INTO `education_tbl` (`id`, `education_level`) VALUES
(2, 'M.Sc');

-- --------------------------------------------------------

--
-- Table structure for table `emails_tbl`
--

CREATE TABLE IF NOT EXISTS `emails_tbl` (
`id` int(11) NOT NULL,
  `emails` varchar(255) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `emails_tbl`
--

INSERT INTO `emails_tbl` (`id`, `emails`) VALUES
(1, 'mazhar@yahoo.com'),
(2, 'title@gmail.com'),
(3, 'Mozammel_Haque@ymail.com'),
(5, 'mazharul@gmail.com'),
(6, 'book@yamil.com');

-- --------------------------------------------------------

--
-- Table structure for table `hobbies_tbl`
--

CREATE TABLE IF NOT EXISTS `hobbies_tbl` (
`id` int(11) NOT NULL,
  `hobbies` varchar(255) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=22 ;

--
-- Dumping data for table `hobbies_tbl`
--

INSERT INTO `hobbies_tbl` (`id`, `hobbies`) VALUES
(11, 'Sport,Reading'),
(12, 'Travelling,Sport'),
(16, 'Sport,Reading'),
(17, 'Gardening,Sport,Reading'),
(18, 'Gardening,Travelling'),
(19, 'Gardening,Reading'),
(21, 'Travelling');

-- --------------------------------------------------------

--
-- Table structure for table `hobby_tbl`
--

CREATE TABLE IF NOT EXISTS `hobby_tbl` (
`id` int(11) NOT NULL,
  `hobbies` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `new_tbl`
--

CREATE TABLE IF NOT EXISTS `new_tbl` (
`id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=53 ;

--
-- Dumping data for table `new_tbl`
--

INSERT INTO `new_tbl` (`id`, `title`) VALUES
(50, 'logic probe'),
(51, 'digital logic'),
(52, 'hello java');

-- --------------------------------------------------------

--
-- Table structure for table `picture_tbl`
--

CREATE TABLE IF NOT EXISTS `picture_tbl` (
`id` int(11) NOT NULL,
  `picture_name` varchar(255) NOT NULL,
  `picture` blob NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `picture_tbl`
--

INSERT INTO `picture_tbl` (`id`, `picture_name`, `picture`) VALUES
(8, 'digital logic', 0x646f776e6c6f61642e6a7067),
(9, 'book', ''),
(10, 'Zaman', ''),
(11, 'Zaman', '');

-- --------------------------------------------------------

--
-- Table structure for table `summary_tbl`
--

CREATE TABLE IF NOT EXISTS `summary_tbl` (
`id` int(11) NOT NULL,
  `company_name` varchar(255) NOT NULL,
  `summary` varchar(255) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `summary_tbl`
--

INSERT INTO `summary_tbl` (`id`, `company_name`, `summary`) VALUES
(9, 'Zaman', 'this is a nice company.'),
(10, 'Maria', 'it was a nice team.');

-- --------------------------------------------------------

--
-- Table structure for table `terms_tbl`
--

CREATE TABLE IF NOT EXISTS `terms_tbl` (
`id` int(11) NOT NULL,
  `terms` varchar(255) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `terms_tbl`
--

INSERT INTO `terms_tbl` (`id`, `terms`) VALUES
(1, 'Agree'),
(2, 'Disagree');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `birthday_tbl`
--
ALTER TABLE `birthday_tbl`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `city_tbl`
--
ALTER TABLE `city_tbl`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `education_tbl`
--
ALTER TABLE `education_tbl`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `emails_tbl`
--
ALTER TABLE `emails_tbl`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hobbies_tbl`
--
ALTER TABLE `hobbies_tbl`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hobby_tbl`
--
ALTER TABLE `hobby_tbl`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `new_tbl`
--
ALTER TABLE `new_tbl`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `picture_tbl`
--
ALTER TABLE `picture_tbl`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `summary_tbl`
--
ALTER TABLE `summary_tbl`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `terms_tbl`
--
ALTER TABLE `terms_tbl`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `birthday_tbl`
--
ALTER TABLE `birthday_tbl`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `city_tbl`
--
ALTER TABLE `city_tbl`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `education_tbl`
--
ALTER TABLE `education_tbl`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `emails_tbl`
--
ALTER TABLE `emails_tbl`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `hobbies_tbl`
--
ALTER TABLE `hobbies_tbl`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `hobby_tbl`
--
ALTER TABLE `hobby_tbl`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `new_tbl`
--
ALTER TABLE `new_tbl`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=53;
--
-- AUTO_INCREMENT for table `picture_tbl`
--
ALTER TABLE `picture_tbl`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `summary_tbl`
--
ALTER TABLE `summary_tbl`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `terms_tbl`
--
ALTER TABLE `terms_tbl`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
